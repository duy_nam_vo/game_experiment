var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

camera.position.z = 9;
scene.add(camera);

var renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

var sceneInit = sceneSetup(scene);
sceneInit.init();

var xMesh = 1;
var yMesh = 1;

var meshF = meshFactory(scene);
for (var i = -xMesh; i < xMesh; i++) {
    for (var j = -yMesh; j < yMesh; j++) {
        var position = { x: INCREMENT * (i * 2.5), y: INCREMENT * (j * 2.5) };
        meshF.createSphere(COLOR[COLOR_ARRAY[getRandomInt(COLOR_ARRAY.length)]], position);
    }
    
}

function moveMesh(direction,speed = 1){
	var selectedDirection = DIRECTION.filter(elem=>elem.name===direction)[0];
    var mesh = scene.children.filter(elem => elem["X_type"] === MESH_TYPE.SPHERE)[0];
    mesh.position.x += selectedDirection.x * (1 / speed);
    mesh.position.y += selectedDirection.y * (1 / speed);
}

document.addEventListener("keydown", function (ev) {
    let { keyCode } = ev;
    switch (keyCode) {
        case KEYS.S:
            moveMesh('down');
            break;
        case KEYS.W:
            moveMesh('up');
            break;
        case KEYS.A:
            moveMesh('left');
            break;
        case KEYS.D:
            moveMesh('right');
            break;
    }
});

var animate = function () {
    requestAnimationFrame(animate);
    scene.children.filter(elem => elem["X_type"] === MESH_TYPE.SPHERE).forEach(function (el) {
        //meshF.moveMesh(el.uuid);
        //meshF.checkCollision(el.uuid);
    });
    renderer.render(scene, camera);
};
animate();