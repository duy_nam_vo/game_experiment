const WIDTH = window.innerWidth;
const HEIGHT = window.innerHeight;
const OFFSET = {x:300/2,y:400/2}
// Set some camera attributes.
const VIEW_ANGLE = 90;
const ASPECT = WIDTH / HEIGHT;
const NEAR = 0.1;
const FAR = 10000;
const KEYS = {LEFT:37,RIGHT:39,UP:38,DOWN:40,A:65,W:87,D:68,S:83};

const LIGHT_POSITION = { x: -10, y: 100, z: 130 };
const RADIUS = 10;
const SEGMENTS = 20;
const RINGS = 20;
const INCREMENT = 10;

const MOUSE_CENTER = {x:463,y:626};
const LIGHT_INCREMENT = 10;

const LIMIT = {min:-1000,max:-100}
// const DIRECTION = {UP:'up',DOWN:'down'}
const ZPOS = -300;

const SIDEWALL_OFFSET = 0.27;
const TOPWALL_OFFSET = 0.27;

var DIRECTION = [];
DIRECTION.push({ name: 'left', x: -1 * INCREMENT, y: 0, idx: 0 });
DIRECTION.push({ name: 'topLeft', x: -1 * INCREMENT, y: INCREMENT, idx: 1 });
DIRECTION.push({ name: 'downLeft', x: -1 * INCREMENT, y: -1 * INCREMENT, idx: 2 });

DIRECTION.push({ name: 'right', x: INCREMENT, y: 0, idx: 3 });
DIRECTION.push({ name: 'topRight', x: INCREMENT, y: INCREMENT, idx: 4 });
DIRECTION.push({ name: 'downRight', x: INCREMENT, y: -1 * INCREMENT, idx: 5 });

DIRECTION.push({name:'up',x:0,y:INCREMENT,idx:6});
DIRECTION.push({name:'down',x:0,y:-1*INCREMENT,idx:7});



const COLOR = {
    RED: '#ff0000',
    BLUE: '#0000ff',
    GREEN: '#00ff00',
    PURPLE: '#800080'
};

const COLOR_ARRAY = ['RED', 'BLUE', 'GREEN', 'PURPLE'];

const COLOR_PROPS = [
    { color: COLOR.RED, speed: 1 },
    { color: COLOR.BLUE, speed: 4 },
    { color: COLOR.GREEN, speed: 3 },
    { color: COLOR.PURPLE, speed: 2 }
];

const MESH_TYPE = {
    WALL: 'wall',
    SPHERE: 'sphere'
};

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
