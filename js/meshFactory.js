var common = {
    utils: function (state) {

        if (!('callbacks' in state)) {
            Object.assign(state, { callbacks: {} });
        }
        return {
            addListener: function (handler, fn) {
                if (typeof fn === 'function') {
                    if (!(handler in state.callbacks)) {
                        state.callbacks[handler] = [];
                    }
                    state.callbacks[handler].push(fn);
                }
            },
            trigger: function (handler) {
                if (handler in state.callbacks) {
                    var arr = [];
                    for (var i = 1; i < arguments.length; i++) {
                        arr.push(arguments[i])
                    }
                    state.callbacks[handler].forEach(function (el) {
                        el(arr.length === 1 ? arr[0] : arr);
                    });
                }
            }
        }
    },
    eventListener: {
        meshCreated:'mesh_created'
    }
}

var sceneFn = {
    buildWall: function (scene) {
        return function () {
            const redMaterial = new THREE.MeshBasicMaterial({ color: COLOR.RED })
                

            const rightWall = new THREE.Mesh(
                new THREE.BoxGeometry(1, HEIGHT, 1), redMaterial
            );

           const leftWall = new THREE.Mesh(
               new THREE.BoxGeometry(1, HEIGHT, 1), redMaterial
            );
            const ceiling = new THREE.Mesh(
                new THREE.BoxGeometry(WIDTH, 1, 1), redMaterial
            );

            const floor = new THREE.Mesh(
                new THREE.BoxGeometry(WIDTH, 1, 1), redMaterial
            );

            rightWall['X_type'] = MESH_TYPE.WALL;
            leftWall['X_type'] = MESH_TYPE.WALL;
            floor['X_type'] = MESH_TYPE.WALL;
            ceiling['X_type'] = MESH_TYPE.WALL;

            rightWall.name='rightWall';
            leftWall.name = 'leftWall';
            ceiling.name = 'ceiling';
            floor.name = 'floor';

            scene.add(rightWall);
            scene.add(leftWall);
            scene.add(ceiling);
            scene.add(floor);

            leftWall.position.z = ZPOS;
            leftWall.position.x = ((WIDTH / 2) - (WIDTH * SIDEWALL_OFFSET)) * -1;

            rightWall.position.z = ZPOS;
            rightWall.position.x = (WIDTH/2) -(WIDTH * SIDEWALL_OFFSET);

            floor.position.z = ZPOS;
            floor.position.y = ((HEIGHT / 2) - (HEIGHT * TOPWALL_OFFSET)) * -1;

            ceiling.position.z = ZPOS;
            ceiling.position.y = ((HEIGHT / 2) - (HEIGHT * TOPWALL_OFFSET));

            //return [leftWall, rightWall, floor, ceiling];
        }
    },
    setupLight: function (scene) {
        return function () {
            const pointLight = new THREE.PointLight(0xffffff, 2);
            pointLight.position.x = LIGHT_POSITION.x;
            pointLight.position.y = LIGHT_POSITION.y;
            pointLight.position.z = LIGHT_POSITION.z;
            scene.add(pointLight);
        }
    },
    init: function (scene) {
        return function () {
            //sceneFn.setupLight(scene)();
            sceneFn.buildWall(scene)();
        }
    }
}

var meshFn = {
    createSphere: function (state) {
        return function (color, position = {x:0,y:0}) {
            let props = COLOR_PROPS.filter(elem => elem.color === color)[0];
            let geometry = new THREE.SphereGeometry(RADIUS, SEGMENTS, RINGS);

            let sphere = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: color }));
            sphere.position.x = position.x;
            sphere.position.y = position.y;
            sphere.position.z = ZPOS;

            Object.defineProperties(sphere, {
                speed: {
                    value: props.speed,
                    writable: false
                }, X_type: {
                    value: MESH_TYPE.SPHERE,
                    writable: false
                }
            });
            state.utils.trigger(common.eventListener.meshCreated, sphere);
            return sphere;
        }
    },
    moveMesh: function (state) {
        return function (meshUUID) {
            var mesh = state.scene.children.filter(elem => elem.uuid === meshUUID)[0];
            if (!('direction' in mesh)) {
                Object.assign(mesh, { direction: DIRECTION[getRandomInt(DIRECTION.length)]});
            }
            mesh.position.x += mesh.direction.x * (1/mesh.speed);
            mesh.position.y += mesh.direction.y *(1/mesh.speed);
        }
    },
    checkCollision: function (state) {
        
        return function (meshUUID) {
            var collidableMesh = state.scene.children.filter(elem => elem.type === "Mesh" && elem.uuid !== meshUUID);

            state.scene.children.filter(elem => elem.uuid === meshUUID && elem.type === "Mesh" && elem["X_type"] !== MESH_TYPE.WALL).forEach(function (el) {

                var originPoint = el.position.clone();
                for (var i = 0; i < collidableMesh.length; i++) {
                    for (var vertexIndex = 0; vertexIndex < el.geometry.vertices.length; vertexIndex++) {
                        var localVertex = el.geometry.vertices[vertexIndex].clone();
                        var globalVertex = localVertex.applyMatrix4(el.matrix);
                        var directionVector = globalVertex.sub(el.position);
                        var ray = new THREE.Raycaster(originPoint, directionVector.clone().normalize());

                        // calculate objects intersecting the picking ray
                        // var intersects = raycaster.intersectObjects( collidableMeshList );
                        var collisionResults = ray.intersectObject(collidableMesh[i]);

                        if (collisionResults.length > 0 && collisionResults[0].distance < directionVector.length()) {
                            var possiblePath = [];

                            switch (collisionResults[0].object.name) {
                                case "leftWall":
                                    possiblePath.push(...DIRECTION.filter(elem => elem.name === "right" || elem.name === "topRight" || elem.name === "downRight"));
                                    break;
                                case "rightWall":
                                    possiblePath.push(...DIRECTION.filter(elem => elem.name === "left" || elem.name === "topLeft" || elem.name === "downLeft"));
                                    break;
                                case "ceiling":
                                    possiblePath.push(...DIRECTION.filter(elem => elem.name === "down" || elem.name === "downRight" || elem.name === "downLeft"));
                                    break;
                                case "floor":
                                    possiblePath.push(...DIRECTION.filter(elem => elem.name === "up" || elem.name === "topRight" || elem.name === "topLeft"));
                                    break;
                            }
                            var mesh = state.scene.children.filter(elem => elem.type === "Mesh" && elem.uuid === meshUUID)[0];
                            if (possiblePath.length > 0) {
                                mesh.direction = possiblePath[getRandomInt(possiblePath.length)];
                            }
                            //console.log(state.scene.children.filter(elem => elem.uuid === meshUUID)[0]);
                            //console.log(collisionResults[0].object);
                            //console.log(" Hit ");
                            break;
                        }
                    }
                }
            });
            
        }
    }
}

var collisionFn = {
    isLeftIntersect: function (state) {
        return function (mesh) {
            var leftWall = state.scene.children.filter(el => el.name === "leftWall")[0];
            var margin = leftWall.position.x * 0.05;
            if (leftWall.position.x + margin < mesh.position.x && mesh.position.x < leftWall.position.x - margin) {
                return true;
            }
            return false;
        }
    },
    isRightIntersect: function (state) {
        return function (mesh) {
            var rightWall = state.scene.children.filter(el => el.name === "rightWall")[0];
            var margin = rightWall.position.x * 0.05;
            if (rightWall.position.x - margin < mesh.position.x && mesh.position.x < rightWall.position.x + margin) {
                return true;
            }
            return false;
        }
    },
    isFloorIntersect: function (state) {
        return function (mesh) {
            var floor = state.scene.children.filter(el => el.name === "floor")[0];
            var margin = floor.position.y * 0.05;

            if (floor.position.y + margin < mesh.position.y && mesh.position.y < floor.position.y - margin) {
                return true;
            }
            return false;
        }
    },
    isCeilingIntersect: function (state) {
        return function (mesh) {
            var ceiling = state.scene.children.filter(el => el.name === "ceiling")[0];
            var margin = ceiling.position.y * 0.05;

            if (ceiling.position.y - margin < mesh.position.y && mesh.position.y < ceiling.position.y + margin) {
                return true;
            }
            return false;
        }
    },
    checkCollision: function (state) {
        return function (meshUUID) {
            let mesh = state.scene.children.filter(elem => elem.uuid === meshUUID)[0];
            let leftIntersect = collisionFn.isLeftIntersect(state)(mesh) ? 1 : 0;
            let rightIntersect = collisionFn.isRightIntersect(state)(mesh) ? 2 : 0;
            let floorIntersect = collisionFn.isFloorIntersect(state)(mesh) ? 4 : 0;
            let ceilingIntersect = collisionFn.isCeilingIntersect(state)(mesh) ? 8 : 0;
            let sumIntersect = leftIntersect + rightIntersect + floorIntersect + ceilingIntersect;
            let possiblePath = [];
            
            switch ((sumIntersect).toString(2).padStart(4,"0")) {
                
                case "0001": //left
                    possiblePath.push(...DIRECTION.filter(elem => elem.name === "right" || elem.name === "topRight" || elem.name === "downRight"));
                    break;
                case "0010": //right
                    possiblePath.push(...DIRECTION.filter(elem => elem.name === "left" || elem.name === "topLeft" || elem.name === "downLeft"));
                    break;
                case "0100": //floor
                    possiblePath.push(...DIRECTION.filter(elem => elem.name === "up" || elem.name === "topRight" || elem.name === "topLeft"));
                    break;
                case "1000": //ceiling
                    possiblePath.push(...DIRECTION.filter(elem => elem.name === "down" || elem.name === "downRight" || elem.name === "downLeft"));
                    break;
                case "0101": //left-floor
                    possiblePath.push(...DIRECTION.filter(elem => elem.name === "topRight"));
                    break;
                case "1001": //left-ceiling
                    possiblePath.push(...DIRECTION.filter(elem => elem.name === "downRight"));
                    break;
                case "0110": //right-floor
                    possiblePath.push(...DIRECTION.filter(elem => elem.name === "topLeft"));

                    break;
                case "1010": //right-ceiling
                    possiblePath.push(...DIRECTION.filter(elem => elem.name === "downLeft"));
                    break;   
            }
            if (possiblePath.length > 0) {
                console.log((sumIntersect).toString(2).padStart(4, "0"));
                mesh.direction = possiblePath[getRandomInt(possiblePath.length - 1)];
            }
        }
    }
}

function sceneSetup(scene) {
    var public = Object.assign({}, { init: sceneFn.init(scene) });
    return public;
}


function meshFactory(scene) {
    var state = Object.assign({}, {
        scene: scene,
        collidableMesh: [],
        walls: []
    });

    Object.assign(state, { utils: common.utils(state) });

    state.utils.addListener(common.eventListener.meshCreated, function (mesh) {
        state.scene.add(mesh);
        state.collidableMesh.push(mesh);
    });

    var public = Object.assign({}, {
        createSphere: meshFn.createSphere(state),
        checkCollision: collisionFn.checkCollision(state),
        moveMesh : meshFn.moveMesh(state)
    });
	return public;
}